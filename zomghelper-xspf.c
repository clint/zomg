#include <stdio.h>
#include <string.h>
#include <libxml/xmlreader.h>

#ifdef LIBXML_READER_ENABLED

#define ZOMG_XSPF_PLAYLIST 1
#define ZOMG_XSPF_TITLE 2
#define ZOMG_XSPF_CREATOR 3
#define ZOMG_XSPF_DURATION 4
#define ZOMG_XSPF_ALBUM 5
#define ZOMG_XSPF_TRACK 6
#define ZOMG_XSPF_TRACKLIST 7
#define ZOMG_XSPF_LOCATION 8
#define ZOMG_XSPF_ID 9
#define ZOMG_XSPF_IMAGE 10
#define ZOMG_XSPF_LASTFM_TRACKAUTH 11

static void
processNode (xmlTextReaderPtr reader)
{
  const xmlChar *name, *value;
  static int level0, level1, level2, level3, trackno;

  name = xmlTextReaderConstName (reader);
  if (name == NULL)
    name = BAD_CAST "--";

  value = xmlTextReaderConstValue (reader);

  switch (xmlTextReaderDepth (reader))
    {
    case 0:
      if (strcmp (name, "playlist"))
	level0 = 0;
      else
	level0 = ZOMG_XSPF_PLAYLIST;
      break;
    case 1:
      if (!strcmp (name, "title"))
	level1 = ZOMG_XSPF_TITLE;
      else if (!strcmp (name, "creator"))
	level1 = ZOMG_XSPF_CREATOR;
      else if (!strcmp (name, "trackList"))
	{
	  level1 = ZOMG_XSPF_TRACKLIST;
	  trackno = 0;
	}
      else
	level1 = 0;
      break;
    case 2:
      if (level0 && level1 && !strcmp (name, "#text"))
	{
	  switch (level1)
	    {
	    case ZOMG_XSPF_TITLE:
	      printf ("playlist_title=%s\n", value);
	      break;
	    case ZOMG_XSPF_CREATOR:
	      printf ("playlist_creator=%s\n", value);
	      break;
	    }
	}
      else if (level0 && (level1 == ZOMG_XSPF_TRACKLIST) &&
	       !strcmp (name, "track") &&
	       (xmlTextReaderNodeType(reader)==1))
	{
	  level2 = ZOMG_XSPF_TRACK;
	  ++trackno;
	}
      break;
    case 3:
      level3 = 0;
      if (level0 && level1 && (level2 == ZOMG_XSPF_TRACK))
	{
	  if (!strcmp (name, "location"))
	    level3 = ZOMG_XSPF_LOCATION;
	  else if (!strcmp (name, "title"))
	    level3 = ZOMG_XSPF_TITLE;
	  else if (!strcmp (name, "id"))
	    level3 = ZOMG_XSPF_ID;
	  else if (!strcmp (name, "album"))
	    level3 = ZOMG_XSPF_ALBUM;
	  else if (!strcmp (name, "creator"))
	    level3 = ZOMG_XSPF_CREATOR;
	  else if (!strcmp (name, "duration"))
	    level3 = ZOMG_XSPF_DURATION;
	  else if (!strcmp (name, "image"))
	    level3 = ZOMG_XSPF_IMAGE;
	  else if (!strcmp (name, "lastfm:trackauth"))
	    level3 = ZOMG_XSPF_LASTFM_TRACKAUTH;
	}
      break;
    case 4:
      if (level0 && level1 && level2 && level3)
	{
	  switch (level3)
	    {
	    case ZOMG_XSPF_LOCATION:
	      printf ("location[%d]=%s\n", trackno, value);
	      break;
	    case ZOMG_XSPF_TITLE:
	      printf ("title[%d]=%s\n", trackno, value);
	      break;
	    case ZOMG_XSPF_ID:
	      printf ("id[%d]=%s\n", trackno, value);
	      break;
	    case ZOMG_XSPF_ALBUM:
	      printf ("album[%d]=%s\n", trackno, value);
	      break;
	    case ZOMG_XSPF_CREATOR:
	      printf ("creator[%d]=%s\n", trackno, value);
	      break;
	    case ZOMG_XSPF_DURATION:
	      printf ("duration[%d]=%s\n", trackno, value);
	      break;
	    case ZOMG_XSPF_IMAGE:
	      printf ("image[%d]=%s\n", trackno, value);
	      break;
	    case ZOMG_XSPF_LASTFM_TRACKAUTH:
	      printf ("lastfm_trackauth[%d]=%s\n", trackno, value);
	      break;
	    }
	}
      break;
    }

}

static void
streamFile (const char *filename)
{
  xmlTextReaderPtr reader;
  int ret;

  reader = xmlReaderForFile (filename, NULL, 0);
  if (reader != NULL)
    {
      ret = xmlTextReaderRead (reader);
      while (ret == 1)
	{
	  processNode (reader);
	  ret = xmlTextReaderRead (reader);
	}
      xmlFreeTextReader (reader);
      if (ret != 0)
	{
	  fprintf (stderr, "%s : failed to parse\n", filename);
	}
    }
  else
    {
      fprintf (stderr, "Unable to open %s\n", filename);
    }
}

int
main (int argc, char **argv)
{
  if (argc != 2)
    return (1);
  LIBXML_TEST_VERSION streamFile (argv[1]);

  xmlCleanupParser ();
  xmlMemoryDump ();
  return (0);
}

#else
int
main (void)
{
  fprintf (stderr, "XInclude support not compiled in\n");
  exit (1);
}
#endif
