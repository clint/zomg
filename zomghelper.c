/*  zomghelper, an information extractor for Ogg Vorbis files
    Copyright (C) 2005-2014  Clint Adams

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <vorbis/vorbisfile.h>
#include <opusfile.h>

struct result {
	int secs;
	vorbis_comment *com;
};

void do_vorbis(OggVorbis_File *f, struct result *r) {
	double k;

	if (ov_test_open(f))
		exit(4);

	k = ov_time_total(f,-1);
	if (k == OV_EINVAL)
		exit(5);

	r->com = ov_comment(f,-1);
	if (r->com == NULL)
		exit(6);

	r->secs = (int)ceil(k);
}

void do_opus(OggOpusFile *f, struct result *r) {
	ogg_int64_t k;

	if (op_test_open(f))
		exit(4);

	k = op_pcm_total(f,-1);
	if (k == OV_EINVAL)
		exit(5);

	r->com = (vorbis_comment *)op_tags(f,-1);
	if (r->com == NULL)
		exit(6);

	r->secs = (int)(k / 48000);
}

int main(int argc, char **argv) {
	FILE *f;
	OggOpusFile *of;
	OggVorbis_File vf;
	struct result r;
	char **soc;

	if (argc!=2)
		exit(1);

	f = fopen(argv[1], "r");
	if (f==NULL)
		exit(2);

	if (!ov_test(f, &vf, NULL, 0))
		do_vorbis(&vf, &r);
	else if (of = op_test_file(argv[1], NULL))
		do_opus(of, &r);
	else
		exit(3);

	printf("ZOMGSECS: %d\n", r.secs);

	soc = r.com->user_comments;

	while(*(soc)) {
		printf("%s\n", *soc++);
	}
}
